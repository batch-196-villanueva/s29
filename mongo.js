// db.products.insertMany([
//     {
//         name: "Iphone X",
//         price: 30000,
//         isActive: true
//         
//     },
//     {
//         name: "Samsung Galaxy S21",
//         price: 51000,
//         isActive: true
// 
//     },
//     {
//         name: "Razer Blackshark V2X",
//         price: 2000,
//         isActive: false
//     },
//     {
//         name: "RAKK Gaming House",
//         price: 1800,
//         isActive: true
//     },
//     {
//         name: "Razer Mechanical Keyboard",
//         price: 4000,
//         isActive: true
//     }
// ])
// 
// db.products.find()

// Query Operators Allows us to expand our queries and define conditions
// instead of just looking for specific values


// $gt, $lt, $gte, $lte

// $gt - query operator which means greater than
// db.products.find({price:{$gt:3000}})

// $lt - query operator which means less than
// db.products.find({price:{$lt:3000}})

// $gte - query operator which means greater than or equal to
// db.products.find({price:{$gte:30000}})

// $lte - query operator which means less than or equal to
// db.products.find({price:{$lte:2800}})



// db.users.insertMany([
//     {
//         firstName: "Mary Jane",
//         lastName: "Watson",
//         email: "mjtiger@gmail.com",
//         password: "tigerjackpot15",
//         isAdmin: false
//     },
//     {
//         firstName: "Gwen",
//         lastName: "Stacy",
//         email: "stacyTech@gmail.com",
//         password: "stacyTech1991",
//         isAdmin: true
//     },
//     {
//         firstName: "Peter",
//         lastName: "Parker",
//         email: "peterWebDev@gmail.com",
//         password: "webdeveloperPeter",
//         isAdmin: true
//     },
//     {
//         firstName: "Jonah",
//         lastName: "Jameson",
//         email: "jjjameson@gmail.com",
//         password: "spideyisamenace",
//         isAdmin: false
//     }, 
//     {
//         firstName: "Otto",
//         lastName: "Octavius",
//         email: "ottoOctopi@gmail.com",
//         password: "doc0ck15",
//         isAdmin: true
//     }
// ])
    
// $regex - query operator which will allow us to find documents which will match the
// characters/pattern of the characters we are looking for.

// $regex looks for documents with partial match and by default is case sensitive

db.users.find({firstName:{$regex:'O'}})

// $options - if used, our regex will be case insensitive
db.users.find({firstName:{$regex:'o',$options:'$i'}})

// You can also find for documents with partial matches
db.products.find({name:{$regex:'phone',$options:'$i'}})

//Find users whose email have the word "web" in it
db.users.find({email:{$regex:'web',$options:'$i'}})

db.products.find({name:{$regex:'razer',$options:'$i'}})
db.products.find({name:{$regex:'rakk',$options:'$i'}})

// $or $and - logical operators - works almost the same way as they do in JS
// $or - allow us to have a logical operation to find for documents which 
// can satisfy at least 1 of our conditions

db.products.find({$or:[{name:{$regex: 'x',$options: '$i'}},{price:{$lte:10000}}]})
db.products.find({$or:[{name:{$regex: 'x',$options: '$i'}},{price:{$gte:10000}}]})

// $and - look or find for documents that satisfies both conditions
db.products.find({$and:[{name: {$regex: 'razer', $options: '$i'}}, {price:{$gte:3000}}]})
db.products.find({$and:[{name: {$x: 'razer', $options: '$i'}}, {price:{$gte:30000}}]})
db.users.find({$and:[{lastName:{$regex: 'w', $options: '$i'}},{isAdmin:false}]})
db.users.find({$and:[{firstName:{$regex: 'a', $options: '$i'}},{isAdmin:true}]})

// Field Projection
// db.collection.find({query},{projection}) - 0 means hide, 1 means show
db.users.find({},{_id:0,password:0})

db.users.find({isAdmin:true},{_id:0,email:1})
// id_field must be complicitly hidden if you want to hide it.
// We can also just pick which fields to show
db.users.find({isAdmin:false},{firstName:1,lastName:1})
db.products.find({price:{$gte:10000}},{_id:0,name:1,price:1})